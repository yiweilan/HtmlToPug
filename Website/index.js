const express = require('express');
const app = express();
const path = require("path");
let package = require('./package.json');

app.use(express.static(path.join(__dirname, 'static')));

app.get('/checkUpload', (req, res) => {
  let updateVersion = package.updateVersion;
  let status = null;
  updateVersion.forEach((val,index) => {
    if(val.version == req.query.v) {
      if(index == updateVersion.length -1) {
        status = {
          code: 200,
          result: "当前已是最新版本"
        }
      } else {
        status = updateVersion[updateVersion.length-1]
      }
    }
  });
  res.json(status)
});

app.listen(9000, () => {
  console.log('Example app listening on port 9000!')
});

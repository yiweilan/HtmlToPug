"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var electron_1 = require("electron");
var index_1 = require("./config/index");
var IndexWindow_1 = tslib_1.__importDefault(require("./module/ipcMain/IndexWindow"));
var menu_1 = tslib_1.__importDefault(require("./module/menu/menu"));
var createWindow = /** @class */ (function () {
    function createWindow() {
        // @ts-ignore
        this.windows = new electron_1.BrowserWindow(index_1.Config.WelcomeConfig);
        this.Start();
    }
    createWindow.prototype.Start = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                menu_1.default.buildFromTemplate(this.windows.webContents);
                this.SetLoadFile();
                this.RegistrationProcess();
                return [2 /*return*/];
            });
        });
    };
    // 加载启动页面
    createWindow.prototype.SetLoadFile = function () {
        this.windows.loadFile(index_1.Config.WelcomePage);
    };
    // 注册IndexWindow的ipc
    createWindow.prototype.RegistrationProcess = function () {
        IndexWindow_1.default.createdWindow(this.windows);
    };
    return createWindow;
}());
exports.createWindow = createWindow;

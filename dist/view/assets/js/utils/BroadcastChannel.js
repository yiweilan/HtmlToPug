class Broadcast {
  constructor() {
    this.fontSetting = new BroadcastChannel('FontBroadcastChannel');
    this.ColorSetting = new BroadcastChannel('ColorBroadcastChannel');
    this.ThemeSetting = new BroadcastChannel('ThemeBroadcastChannel')
  }
}

window.$broadCast = new Broadcast()

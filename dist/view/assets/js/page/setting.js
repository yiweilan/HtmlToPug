
class Setting {
  constructor() {
    this.tabsTitle = $(".tabs-title span");
    this.tabsContent = $(".tabs-content .tabs-item");

    $utils.changeTheme($ipc.getCurrentTheme());


    $(".version_num").text(`您当前的版本：v${$ipc.getCurrentVersion()}`);

    this.ConfigTabs();
    this.ConfigTheme();

    this.currentFontSelection();
    this.currentColorSelection();
    this.currentLoginStatus();

    this.MonitorFontSizeSelect();
    // this.MonitorColorSelect();

    this.closedSetting();
    this.LoginChecked();

    this.checkUpdateClick();

    this.onColorChange();
  }

  /**
   * 配置Tabs切换
   * @constructor
   */
  ConfigTabs() {
    this.tabsTitle.each((index,val) => {
      $(val).on('click',() =>{
        $(val).addClass("active-title").siblings().removeClass("active-title")
        $(this.tabsContent[index]).addClass("active-content").siblings().removeClass("active-content")
      });
    });
  }

  /**
   * 渲染主题列表，绑定点击事件，
   * 存储点击对象下标，通知首页改变样式
   * @constructor
   */
  ConfigTheme() {
    let self = this;
    let currentIndex = localStorage.getItem("themeIndex") == null
                       ? 0
                       : localStorage.getItem("themeIndex");

    $config.themeColor.forEach((val,index) => {
      $(".theme-color ul").append(`<li title="点击使用主题：[${val.name}]。注意：这会覆盖自定义主题。">
                                      ${index == currentIndex ? '<span>♥️</span>' : ''}
                                      <div class="pf">
                                       <div class="them" style="background: ${val.left}"></div>
                                       <div class="them" style="background: ${val.right}"></div>
                                      </div>
                                      <p style="color:${val.color}">${val.name}</p>
                                   </li>
                                  `);
    });

    $(".theme-color ul li").on("click", function () {

      $(".theme-color ul li span").each((index,val) => $(val).remove());
      $(this).prepend(`<span>♥️</span>`);

      // 如果用户使用系统提供的主题色，那么删除用户自定义的颜色方案
      self.onColorChange($config.themeColor[$(this).index()])

      // 如果用户使用系统提供的主题色，那么同步修改自定义主题色的默认数值
      $utils.changeCustomTheme($config.themeColor[$(this).index()]);

      // 本地保存用户选中的系统主题色下标
      localStorage.setItem("themeIndex", $(this).index())
      $broadCast.ThemeSetting.postMessage($config.themeColor[$(this).index()])
    })
  }

  /**
   * 配置设置页面字体select默认选中
   */
  currentFontSelection() {
    if(localStorage.getItem("currentFont") !== null){
      $(".font_size").val(localStorage.getItem("currentFont"));
    }
  }

  /**
   * 配置设置页面color select默认选中
   */
  currentColorSelection() {
    if(localStorage.getItem("currentColor") !== null){
      $(".color_setting").val(localStorage.getItem("currentColor"));
    }
  }

  /**
   * 配置 开机自动 的默认选中
   */
  currentLoginStatus() {
    if(localStorage.getItem("isLoginStart") !== null){
      let statusLogin = localStorage.getItem("isLoginStart");
      statusLogin = statusLogin == "false" ? false : true;
      $(".isLogin").prop("checked", statusLogin);
    }
  }

  /**
   * select 选择框选中的时候发送广播通知index页面做出及时修改
   * 并本地保存用户选中数值
   * @constructor
   */
  MonitorFontSizeSelect() {
    $('.font_size').change(function (e) {
      localStorage.setItem("currentFont", $(this).val())
      $broadCast.fontSetting.postMessage($(this).val());
    });
  }

  /**
   * 字体颜色被选择的时候触发
   * @constructor
   */
  // MonitorColorSelect() {
  //   $('.color_setting').change(function (e) {
  //     localStorage.setItem("currentColor", $(this).val())
  //     $broadCast.ColorSetting.postMessage($(this).val());
  //   });
  // }

  /**
   * 监听开机自启的checkbox
   * @constructor
   */
  LoginChecked() {
    $(".isLogin").change(function() {
      localStorage.setItem("isLoginStart", $(".isLogin").is(':checked'))
      $ipc.isLoginStart($(".isLogin").is(':checked'))
    });
  }

  /**
   * 关闭设置页面
   */
  closedSetting() {
    $(".closed").on("click",function () {
      $ipc.closedSetting()
    })
  }

  /**
   * 检测升级按钮被点击
   */
  checkUpdateClick() {
    $(".version_check").click( () => {
      // 触发后端检查更新
      let res = JSON.parse($ipc.checkUpdate());

      // 判断是否需要更新，如果有code，已是最新版
      if(res.hasOwnProperty("code")) {
        $(".version_check").text(`检查更新 ${res.result}`);

      // 弹窗提示用户新版本
      } else {
        // 清空上次弹窗记录
        $("#openUpdate ul").empty();
        $("#openUpdate p").text();
        $(".dowNew").text();

        $("#openUpdate p").text(`最新版本 ${res.version}`);
        $(".dowNew").text("下载最新版");

        // 显示更新日志
        res.introduce.forEach(val => $("#openUpdate ul").append(` <li> ${val.id}：${val.text} </li> `));

        // 打开弹窗
        syalert.syopen('openUpdate');

        this.downLoadNewVersion(res.downUrl)
      }
    })
  }

  /**
   * 下载最新版 按钮被点击
   */
  downLoadNewVersion(url) {
    $(".dowNew").click( () => $ipc.downNew(url));
  }

  /**
   * 自定义主题颜色选择器被选中的时候
   */
  onColorChange(defaultTheme = null) {
    let CustomTheme = null;
    if (defaultTheme == null) {
      // 如果用户使用了 默认提供主题
      if(localStorage.getItem("themeIndex") !== null) {
        // 并且同时也还使用了自定义主题
        if(localStorage.getItem("CustomTheme") !== null) {

          // 读取自定义主题并保存
          CustomTheme = JSON.parse(localStorage.getItem("CustomTheme"));
          $utils.changeCustomTheme(CustomTheme)

          // 用户只是用了默认主题
        } else {
          CustomTheme = $config.themeColor[localStorage.getItem("themeIndex")]
          $utils.changeCustomTheme(CustomTheme)
        }
        // 如果默认主题也没用，那么读取默认主题第一个数据
      }
    } else {
      CustomTheme = defaultTheme;
    }


    console.log("CustomTheme: ", CustomTheme)

    // 左边颜色
    $(".leftColor").change(function() {
      CustomTheme.left = $(this).val()
      console.log("left CustomTheme: ", CustomTheme)
      localStorage.setItem("CustomTheme", JSON.stringify(CustomTheme))
      $broadCast.ThemeSetting.postMessage(CustomTheme)
    });

    // 右边颜色
    $(".rightColor").change(function(){
      CustomTheme.right = $(this).val()
      console.log("right CustomTheme: ", CustomTheme)
      localStorage.setItem("CustomTheme", JSON.stringify(CustomTheme))
      $broadCast.ThemeSetting.postMessage(CustomTheme)
    });

    // 字体颜色
    $(".fontColor").change(function(){
      CustomTheme.color = $(this).val()
      localStorage.setItem("CustomTheme", JSON.stringify(CustomTheme))
      $broadCast.ThemeSetting.postMessage(CustomTheme)
    });


  }
}

new Setting();








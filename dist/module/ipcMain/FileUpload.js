"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var electron_1 = require("electron");
var utils_1 = tslib_1.__importDefault(require("../utils/utils"));
var index_1 = require("../../config/index");
var FileUpload = /** @class */ (function () {
    function FileUpload() {
    }
    /**
     * 文件选择的进程处理
     */
    FileUpload.prototype.upload = function () {
        var _this = this;
        electron_1.ipcMain.on('openFile', function (event, arg) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
            var params, _a;
            return tslib_1.__generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        params = arg == "html" ? index_1.Config.FileType.HtmlFileConfig : index_1.Config.FileType.PugFileConfig;
                        // 使用系统自带文件选择对话框选中文件，获取选中文件路径
                        _a = event;
                        return [4 /*yield*/, utils_1.default.OpenDialog(params)];
                    case 1:
                        // 使用系统自带文件选择对话框选中文件，获取选中文件路径
                        _a.returnValue = _b.sent();
                        return [2 /*return*/];
                }
            });
        }); });
    };
    return FileUpload;
}());
exports.default = new FileUpload();

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var electron_1 = require("electron");
var utils_1 = tslib_1.__importDefault(require("../utils/utils"));
var index_1 = require("../../config/index");
var electron_2 = require("electron");
var SettingWindow_1 = tslib_1.__importDefault(require("../../module/ipcMain/SettingWindow"));
var action_1 = tslib_1.__importDefault(require("../touchbar/action"));
var menu_1 = tslib_1.__importDefault(require("../menu/menu"));
var FileUpload_1 = tslib_1.__importDefault(require("./FileUpload"));
var ThemeChange_1 = tslib_1.__importDefault(require("./ThemeChange"));
var LoginStart_1 = tslib_1.__importDefault(require("./LoginStart"));
var WindowAction_1 = tslib_1.__importDefault(require("./WindowAction"));
var IndexWindow = /** @class */ (function () {
    function IndexWindow() {
    }
    /**
     * 创建主页窗口
     * @param welcomeWindow
     */
    IndexWindow.prototype.createdWindow = function (welcomeWindow) {
        var _this = this;
        electron_1.ipcMain.on('openIndexWindow', function (event, arg) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                this.IndexWin = new electron_2.BrowserWindow(index_1.Config.WindowsConfig);
                this.IndexWin.loadFile(index_1.Config.StartUpPage);
                // 在打开主页前关闭欢迎页，
                // 注册各种Ipc
                welcomeWindow.close();
                this.RegistrationProcess();
                this.IndexWin.once('ready-to-show', function () {
                    _this.IndexWin.show();
                });
                event.returnValue = "success";
                return [2 /*return*/];
            });
        }); });
    };
    /**
     * 注册首页需要的一些Ipc
     * @constructor
     */
    IndexWindow.prototype.RegistrationProcess = function () {
        WindowAction_1.default.action(this.IndexWin);
        // 文件上传主进程
        FileUpload_1.default.upload();
        // 菜单进程
        menu_1.default.buildFromTemplate(this.IndexWin.webContents);
        // touchbar进场
        this.IndexWin.setTouchBar(action_1.default.getMyTouchBarItem(this.IndexWin.webContents));
        /**
         * 设置页面
         */
        // 文件下载监听
        utils_1.default.downFile(this.IndexWin);
        // 创建设置页面
        SettingWindow_1.default.createdWindow(this.IndexWin);
        // 下载新版 ipc
        SettingWindow_1.default.downVersion(this.IndexWin);
        // 订阅macOS的原生的主题切换通知
        ThemeChange_1.default.MacOsThemeChange();
        // 开机自启
        LoginStart_1.default.openLoginStart();
    };
    return IndexWindow;
}());
exports.default = new IndexWindow();

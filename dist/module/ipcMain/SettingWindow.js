"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var electron_1 = require("electron");
var utils_1 = tslib_1.__importDefault(require("../utils/utils"));
var index_1 = require("../../config/index");
var electron_2 = require("electron");
var net_1 = tslib_1.__importDefault(require("../../module/net/net"));
var SettingWindow = /** @class */ (function () {
    function SettingWindow() {
        this.closedSetting();
        this.check();
        this.getVersion();
    }
    SettingWindow.prototype.createdWindow = function (parentWindow) {
        var _this = this;
        electron_1.ipcMain.on('openWindow', function (event, arg) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                // @ts-ignore
                index_1.Config.SettingWindow.parent = parentWindow;
                this.SettingWin = new electron_2.BrowserWindow(index_1.Config.SettingWindow);
                // @ts-ignore
                this.SettingWin.loadFile(index_1.Config.SettingPage);
                this.SettingWin.once('ready-to-show', function () {
                    _this.SettingWin.show();
                });
                // setting.webContents.openDevTools();
                event.returnValue = "success";
                return [2 /*return*/];
            });
        }); });
    };
    /**
     * 下载最新版 被点击
     * @param indexWindow
     */
    SettingWindow.prototype.downVersion = function (indexWindow) {
        var _this = this;
        electron_1.ipcMain.on('downVersion', function (event, arg) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
            var SavePath;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, utils_1.default.SaveDialog(this.SettingWin, {
                            title: '下载提示',
                            message: '请选择软件的保存路径',
                            filters: [
                                { name: 'All', extensions: ['*'] },
                            ],
                            properties: ['openDirectory']
                        })];
                    case 1:
                        SavePath = _a.sent();
                        if (!SavePath.canceled) {
                            index_1.Config.TemporarySavePath = SavePath.filePath;
                            indexWindow.webContents.downloadURL(arg);
                        }
                        event.returnValue = "success";
                        return [2 /*return*/];
                }
            });
        }); });
    };
    /**
     * 检测更新 被点击
     */
    SettingWindow.prototype.check = function () {
        var _this = this;
        electron_1.ipcMain.on('update', function (event, arg) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
            var _a;
            return tslib_1.__generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = event;
                        return [4 /*yield*/, net_1.default.get("" + index_1.Config.UpdateUrl + index_1.Config.currentVersion)];
                    case 1:
                        _a.returnValue = _b.sent();
                        return [2 /*return*/];
                }
            });
        }); });
    };
    /**
     * 获取客户端默认版本号
     */
    SettingWindow.prototype.getVersion = function () {
        var _this = this;
        electron_1.ipcMain.on("getVersion", function (event, arg) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                event.returnValue = index_1.Config.currentVersion;
                return [2 /*return*/];
            });
        }); });
    };
    /**
     * 关闭设置页面
     */
    SettingWindow.prototype.closedSetting = function () {
        var _this = this;
        electron_1.ipcMain.on('closedWindow', function (event) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                this.SettingWin.close();
                event.returnValue = "success";
                return [2 /*return*/];
            });
        }); });
    };
    return SettingWindow;
}());
exports.default = new SettingWindow();

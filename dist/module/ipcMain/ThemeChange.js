"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var electron_1 = require("electron");
var electron_2 = require("electron");
var ThemeChange = /** @class */ (function () {
    function ThemeChange() {
    }
    ThemeChange.prototype.MacOsThemeChange = function () {
        var _this = this;
        electron_2.ipcMain.on('getCurrentTheme', function (event, arg) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                event.returnValue = electron_1.nativeTheme.shouldUseDarkColors;
                return [2 /*return*/];
            });
        }); });
        // systemPreferences.subscribeNotification(
        //   'AppleInterfaceThemeChangedNotification',
        //   function theThemeHasChanged () {
        //     console.log("主题切换的时候自动触发: ", nativeTheme.shouldUseDarkColors);
        //     webContents.send('updateAppTheme', nativeTheme.shouldUseDarkColors)
        //   }
        // );
    };
    return ThemeChange;
}());
exports.default = new ThemeChange();

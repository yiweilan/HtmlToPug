"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var electron_1 = require("electron");
var TouchBarLabel = electron_1.TouchBar.TouchBarLabel, TouchBarButton = electron_1.TouchBar.TouchBarButton, TouchBarSpacer = electron_1.TouchBar.TouchBarSpacer;
var utils_1 = tslib_1.__importDefault(require("../utils/utils"));
var config_1 = require("../../config");
// 主要负责创建 TouchBar 的类
var MyTouchBar = /** @class */ (function () {
    function MyTouchBar() {
        this.setTouchBarButton();
    }
    // @ts-ignore
    MyTouchBar.prototype.setTouchBarButton = function (title, callback) {
        return new TouchBarButton({
            label: title,
            backgroundColor: '#3a3a3c',
            click: function () { return callback(); }
        });
    };
    /**
     * touchbar 触控栏按钮
     * @param webContents 主进程传递过来的 webContents
     * touchbar上按钮被点击后，会调用webContents推送主进程数据到渲染进程
     */
    MyTouchBar.prototype.getMyTouchBarItem = function (webContents) {
        var _this = this;
        return new electron_1.TouchBar({
            items: [
                new TouchBarSpacer({ size: 'small' }),
                this.setTouchBarButton("打开HTML文件", function () { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                    var _a, _b, _c;
                    return tslib_1.__generator(this, function (_d) {
                        switch (_d.label) {
                            case 0:
                                // @ts-ignore
                                _b = (_a = webContents).send;
                                _c = ['TouchBarHtmlFileChoice'];
                                return [4 /*yield*/, utils_1.default.OpenDialog(config_1.Config.FileType.HtmlFileConfig)];
                            case 1:
                                // @ts-ignore
                                _b.apply(_a, _c.concat([_d.sent()]));
                                return [2 /*return*/];
                        }
                    });
                }); }),
                new TouchBarSpacer({ size: 'small' }),
                this.setTouchBarButton("打开Pug文件", function () { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                    var _a, _b, _c;
                    return tslib_1.__generator(this, function (_d) {
                        switch (_d.label) {
                            case 0:
                                // @ts-ignore
                                _b = (_a = webContents).send;
                                _c = ['TouchBarPugFileChoice'];
                                return [4 /*yield*/, utils_1.default.OpenDialog(config_1.Config.FileType.PugFileConfig)];
                            case 1:
                                // @ts-ignore
                                _b.apply(_a, _c.concat([_d.sent()]));
                                return [2 /*return*/];
                        }
                    });
                }); }),
                new TouchBarSpacer({ size: 'small' }),
                this.setTouchBarButton("字体设置", function () {
                    electron_1.dialog.showErrorBox("字体设置", "内容");
                }),
                new TouchBarSpacer({ size: 'small' }),
                this.setTouchBarButton("关于作者", function () { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                    return tslib_1.__generator(this, function (_a) {
                        switch (_a.label) {
                            case 0: return [4 /*yield*/, electron_1.shell.openExternal(config_1.Config.Github)];
                            case 1:
                                _a.sent();
                                return [2 /*return*/];
                        }
                    });
                }); })
            ]
        });
    };
    return MyTouchBar;
}());
exports.default = new MyTouchBar();

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var electron_1 = require("electron");
var CheckUpdate = /** @class */ (function () {
    function CheckUpdate() {
    }
    CheckUpdate.prototype.get = function (Url) {
        var request = electron_1.net.request({
            method: 'POST',
            url: Url
        });
        return new Promise(function (resolve, reject) {
            request.on('response', function (response) {
                response.on('data', function (chunk) {
                    resolve(chunk.toString());
                });
                response.on('end', function () {
                    console.log('No more data in response.');
                });
            });
            request.end();
        });
    };
    return CheckUpdate;
}());
exports.default = new CheckUpdate();

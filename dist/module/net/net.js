"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var electron_1 = require("electron");
var utils_1 = tslib_1.__importDefault(require("../utils/utils"));
var Net = /** @class */ (function () {
    function Net() {
    }
    Net.prototype.get = function (Url) {
        var request = electron_1.net.request({
            method: 'GET',
            url: Url
        });
        return new Promise(function (resolve, reject) {
            request.on('response', function (response) {
                response.on('data', function (chunk) {
                    resolve(chunk.toString());
                });
                response.on('error', function (error) {
                    utils_1.default.showErrorBox({ title: 'error', content: JSON.stringify(error) });
                });
                response.on('end', function () {
                    console.log('No more data in response.');
                });
            });
            request.end();
        });
    };
    return Net;
}());
exports.default = new Net();

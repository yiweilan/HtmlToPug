class Config {
  constructor() {
    this.themeColor = [
      { id: 1, name: '默认白', left: '#ffffff', right: '#f5f5f5', color: '#000000'},
      { id: 2, name: '默认黑', left: '#2b2b2b', right: '#2b2b2b', color: '#929191'},
      { id: 3, name: '治愈绿', left: '#C5DF56', right: '#96CA00', color: '#e6e6e6'},
      { id: 4, name: '粉戴灰', left: '#B4BAC6', right: '#E5BAB7', color: '#34395F'},
      { id: 5, name: '活力橙', left: '#FEFEFF', right: '#FFA40B', color: '#000000'},
      { id: 6, name: '暗淡灰', left: '#FBDDC1', right: '#FACEA7', color: '#F3BD91'}
    ]
  }
}

window.$config = new Config();

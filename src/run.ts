import { app, ipcMain} from "electron";
import { createWindow }  from "./main";


app.on('ready', () => {
  new createWindow()
});

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
});

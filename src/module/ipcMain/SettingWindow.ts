import { ipcMain } from "electron";
import utils from "../utils/utils";
import { Config } from "../../config/index";
import { app, BrowserWindow } from "electron";
import Net from "../../module/net/net";

class SettingWindow {
  public SettingWin: any;

  constructor() {
    this.closedSetting();
    this.check();
    this.getVersion();
  }

  createdWindow(parentWindow: any) {
    ipcMain.on('openWindow', async (event, arg) => {
      // @ts-ignore
      Config.SettingWindow.parent = parentWindow;
      this.SettingWin = new BrowserWindow(Config.SettingWindow);
      // @ts-ignore
      this.SettingWin.loadFile(Config.SettingPage);
      this.SettingWin.once('ready-to-show', () => {
        this.SettingWin.show()
      });
      // setting.webContents.openDevTools();
      event.returnValue = "success"
    })
  }

  /**
   * 下载最新版 被点击
   * @param indexWindow
   */
  downVersion(indexWindow: any) {
    ipcMain.on('downVersion', async (event, arg) => {
      let SavePath = await utils.SaveDialog(this.SettingWin, {
        title: '下载提示',
        message: '请选择软件的保存路径',
        filters: [
          { name: 'All', extensions: ['*'] },
        ],
        properties: ['openDirectory']
      });

      if(!SavePath.canceled) {
        Config.TemporarySavePath = SavePath.filePath;
        indexWindow.webContents.downloadURL(arg);
      }

      event.returnValue = "success";
    })
  }


  /**
   * 检测更新 被点击
   */
  check() {
    ipcMain.on('update', async (event, arg) => {
      event.returnValue = await Net.get(`${Config.UpdateUrl}${Config.currentVersion}`)
    })
  }

  /**
   * 获取客户端默认版本号
   */
  getVersion () {
    ipcMain.on("getVersion", async (event, arg) => {
      event.returnValue = Config.currentVersion;
    })
  }

  /**
   * 关闭设置页面
   */
  closedSetting() {
    ipcMain.on('closedWindow', async (event) => {
      this.SettingWin.close();
      event.returnValue = "success"
    })
  }


}

export default new SettingWindow();

import { app, BrowserWindow, ipcMain } from "electron";
import path from "path";

class LoginStart {

  openLoginStart() {
    ipcMain.on('settingLoginStart', async (event, arg) => {
      const appFolder = path.dirname(process.execPath)
      const updateExe = path.resolve(appFolder, '..', 'PugToHtml.app')
      const exeName = path.basename(process.execPath)

      app.setLoginItemSettings({
        openAtLogin: arg,
        path: updateExe,
        args: [
          '--processStart', `"${exeName}"`,
          '--process-start-args', `"--hidden"`
        ]
      });

      event.returnValue = "success"

    })


  }

}

export default new LoginStart();

import { ipcMain } from "electron";
import utils from "../utils/utils";
import { Config } from "../../config/index";

class FileUpload {
    /**
     * 文件选择的进程处理
     */
    upload() {
        ipcMain.on('openFile', async (event, arg) => {
          // 决定用哪套配置去选择文件
          // @ts-ignore
          let params = arg == "html" ? Config.FileType.HtmlFileConfig : Config.FileType.PugFileConfig
          // 使用系统自带文件选择对话框选中文件，获取选中文件路径
          event.returnValue = await utils.OpenDialog(params)
        })
    }
}

export default new FileUpload();

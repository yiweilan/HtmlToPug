import { app, ipcMain} from "electron";

class WindowAction {

  /**
   * 自定义窗口关闭，最小化，最大化
   * @param win
   */
  action(win: any) {
    ipcMain.on('min', e=> win.minimize());
    ipcMain.on('max', e=> {
      if (win.isMaximized()) {
        win.unmaximize()
      } else {
        win.maximize()
      }
    });
    ipcMain.on('close', e=> win.close());
  }

}

export default new WindowAction();

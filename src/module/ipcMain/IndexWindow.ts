import { ipcMain } from "electron";
import utils from "../utils/utils";
import { Config } from "../../config/index";
import { app, BrowserWindow } from "electron";
import SettingWindow from "../../module/ipcMain/SettingWindow";
import MyTouchBar from "../touchbar/action";
import CreatedMenu  from "../menu/menu";
import FileIpc from "./FileUpload";
import ThemeChange from "./ThemeChange";
import LoginStart from "./LoginStart";
import WindowAction from "./WindowAction";

class IndexWindow {
  public IndexWin: any;

  /**
   * 创建主页窗口
   * @param welcomeWindow
   */
  createdWindow(welcomeWindow: any) {
    ipcMain.on('openIndexWindow', async (event, arg) => {
      this.IndexWin = new BrowserWindow(Config.WindowsConfig);
      this.IndexWin.loadFile(Config.StartUpPage);
      // 在打开主页前关闭欢迎页，
      // 注册各种Ipc
      welcomeWindow.close();
      this.RegistrationProcess();
      this.IndexWin.once('ready-to-show', () => {
        this.IndexWin.show()
      });
      event.returnValue = "success"
    })
  }

  /**
   * 注册首页需要的一些Ipc
   * @constructor
   */
  public RegistrationProcess(): void {
    WindowAction.action(this.IndexWin);
    // 文件上传主进程
    FileIpc.upload();
    // 菜单进程
    CreatedMenu.buildFromTemplate(this.IndexWin.webContents);
    // touchbar进场
    this.IndexWin.setTouchBar(MyTouchBar.getMyTouchBarItem(this.IndexWin.webContents));
    /**
     * 设置页面
     */
    // 文件下载监听
    utils.downFile(this.IndexWin)
    // 创建设置页面
    SettingWindow.createdWindow(this.IndexWin);
    // 下载新版 ipc
    SettingWindow.downVersion(this.IndexWin);
    // 订阅macOS的原生的主题切换通知
    ThemeChange.MacOsThemeChange();
    // 开机自启
    LoginStart.openLoginStart();
  }

}

export default new IndexWindow();

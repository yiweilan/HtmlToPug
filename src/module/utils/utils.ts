import { dialog, Notification } from "electron";
import { readFile } from "fs";
import { Config } from "../../config/index";
import { join } from "path";
class utils {
  /**
   * 打开弹窗获取用户选中文件路径
   * 并调用 ReadFile 方法获取文件内容
   * @param params Object 弹窗的参数配置
   * @constructor
   */
    async OpenDialog(params: Object): Promise<String>{
        return await this.ReadFile((await dialog.showOpenDialog(params)).filePaths[0]);
    }

    async SaveDialog(parent: any,params: Object): Promise<any> {
      return await dialog.showSaveDialog(params);
    }

    async showErrorBox(params: { title: string, content: string }): Promise<void> {
        await dialog.showErrorBox(params.title, params.content);
    }

    /**
     * 读取指定路径的文件内容
     * @param path string 路径地址
     */
    async ReadFile(path: string): Promise<any> {
        if(path != undefined ) {
            return new Promise((resolve,reject)=>{
                readFile(path, 'utf-8', function (err, data) {
                    if (err) {
                        reject(err)
                    }
                    resolve(data.toString());
                });
            })
        } else {
            this.showErrorBox({ title: '错误', content: '您未选择任何文件，请选择！' })
        }

    }

    downFile(windows: any): void {
      windows.webContents.session.on('will-download', (event: any, item: any, webContents: any) => {
        item.setSavePath(`${Config.TemporarySavePath}/${item.getFilename()}`);

        item.on('updated', (event: any, state: any) => {
          if (state === 'interrupted') {
            console.log('下载被中断，但可以继续')
          } else if (state === 'progressing') {
            // 显示进度条
            windows.setProgressBar(item.getReceivedBytes() / item.getTotalBytes())
            if (item.isPaused()) {
              console.log('下载已暂停')
            } else {
            }
          }
        });

        item.once('done', (event: any, state: any) => {
          if (state === 'completed') {
            // 下载成功后显示通知
            let not = new Notification({
              title: '下载提示',
              body: `文件 ${item.getFilename()} 已成功下载，请及时安装！！`,
              silent: true,
              icon: join(__dirname,'../../view/assets/img/pic.png'),
              sound: join(__dirname, '../../view/assets/audio/Ping.aiff')
            });
            not.show();
            windows.setProgressBar(-1);

          } else {
            console.log(`下载失败: ${state}`)
          }
        })

      })
    }
}

export default new utils();

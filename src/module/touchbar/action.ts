import { app, BrowserWindow, TouchBar, dialog, shell } from "electron";
const { TouchBarLabel, TouchBarButton,TouchBarSpacer } = TouchBar;
import utils from "../utils/utils";
import { Config } from "../../config";

// 主要负责创建 TouchBar 的类
class MyTouchBar {
  constructor() {
      this.setTouchBarButton()
  }

  // @ts-ignore
  setTouchBarButton(title?: string, callback?: any): TouchBarButton {
        return new TouchBarButton({
            label: title,
            backgroundColor: '#3a3a3c',
            click: () => callback()
        });
    }

  /**
   * touchbar 触控栏按钮
   * @param webContents 主进程传递过来的 webContents
   * touchbar上按钮被点击后，会调用webContents推送主进程数据到渲染进程
   */
  getMyTouchBarItem(webContents: any): TouchBar {
        return new TouchBar({
            items: [
                new TouchBarSpacer({ size: 'small' }),
                this.setTouchBarButton("打开HTML文件", async () => {
                  // @ts-ignore
                  webContents.send('TouchBarHtmlFileChoice', await utils.OpenDialog(Config.FileType.HtmlFileConfig))
                }),
                new TouchBarSpacer({ size: 'small' }),
                this.setTouchBarButton("打开Pug文件", async () => {
                  // @ts-ignore
                  webContents.send('TouchBarPugFileChoice', await utils.OpenDialog(Config.FileType.PugFileConfig))
                }),
                new TouchBarSpacer({ size: 'small' }),
                this.setTouchBarButton("字体设置", function () {
                    dialog.showErrorBox("字体设置", "内容")
                }),
                new TouchBarSpacer({ size: 'small' }),
                this.setTouchBarButton("关于作者", async () => {
                  await shell.openExternal(Config.Github)
                })
            ]
        });
    }
}

export default new MyTouchBar();
